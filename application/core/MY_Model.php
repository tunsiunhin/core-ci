<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Model extends CI_Model  {
	public $user_id;
	public $user_email;
	public function __construct()
	{
		parent::__construct();
		$this->check_login();
		
 	}
 	public function check_login()
	{
		if(!$this->session->userdata('user_id') || !$this->session->userdata('user_email')){
			$this->unauthorized();
		}
		$this->user_id    = $this->session->userdata('user_id');
		$this->user_email = $this->session->userdata('user_email');
	}
	
	public function unauthorized()
	{
		header('HTTP/1.1 401 Unauthorized', true, 401);
		$veriry_url = urlencode(base_url().'authentication');
		echo'<script>';
			echo'location.href="https://auth.fptultimate.com?redirect='.$veriry_url.'"';
		echo'</script>';
		die;		
	}
 }
	
?>