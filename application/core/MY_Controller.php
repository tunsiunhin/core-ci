<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller{
	public $data = array();
	public function __construct()
	{
		parent::__construct();
 	}
	public function view($view)
	{
		$this->data['view'] = $view;
		$this->load->view('template/main',$this->data);
	}
}
?>