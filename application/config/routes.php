<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = 'main/dashboard';
$route['404_override'] 		 = "";
$route['authentication']	 = "member/authentication";
$route['logout']			 = "member/logout";

/* End of file routes.php */
/* Location: ./application/config/routes.php */