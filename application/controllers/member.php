<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Member extends CI_Controller{
	
	public function __construct()
	{
		parent::__construct();
	}	
	public function authentication()
	{	
		$auth_token = 'i1eZRPef5QEmyBfWM1Tx';
		$info  = $this->input->get('info');
		$token = $this->input->get('token');
		$verify = sha1($auth_token.$info);
		if(strcmp($token,$verify) == 0)
		{			
			$user_info = json_decode(base64_decode($info),true);
			$this->session->set_userdata('user_id',intval($user_info['user_id'])); 
			$this->session->set_userdata('user_email',$user_info['user_email']);
			redirect(base_url());
		}
		else{
			exit('Unauthorized');
		}
		
	}
	public function auth_facebook($id_user){
		
			$url = 'https://www.facebook.com/dialog/oauth?client_id='.APP_ID.'&redirect_uri='.base_url().'/facebook-login&scope=public_profile,email,publish_pages,manage_pages,read_insights';
				redirect($url);
	}
	public function logout()
	{	
		$this->session->unset_userdata('user_id','user_email');
		$redirect = urlencode(base_url().'authentication');
		redirect('https://auth.fptultimate.com/logout?redirect='.$redirect);
	}
}
?>
		